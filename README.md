# 基于安卓的校园拍卖系统

#### 介绍
基于安卓的校园拍卖系统
使用IDEA开发、SQLite作为数据库、支付宝沙盒作为支付工具
系统具有用户登录注册、管理员登录、发布商品、商品竞价、商品审核、支付尾款等功能

#### 安装教程

APK下载地址：https://gitee.com/stars298281/Android-based-campus-auction-system/blob/master/apk/release/app-release.apk

#### 使用说明

1.  管理员用户名和密码均为root
2.  提前插入三个用户 星星、111、222密码均为111
3.  支付宝支付需要下载支付宝沙箱版
4.  在AliPlayOfSandboxActivity中将自己的支付宝沙箱应用APPID填入APPID
5.  在AliPlayOfSandboxActivity中将自己的商家PID填入PID
5.  在AliPlayOfSandboxActivity中将自己的支付宝沙箱应用公钥填入RSA2_PRIVATE

#### 项目截图
##### 1.  选择用户，登录用户1

![输入图片说明](Project_screenshot/001image.png)

##### 2.  填写信息并发布商品

![输入图片说明](Project_screenshot/002image.png)
![输入图片说明](Project_screenshot/003image.png)

##### 3.  登录管理员端进行审核

![输入图片说明](Project_screenshot/004image.png)
![输入图片说明](Project_screenshot/005image.png)
![输入图片说明](Project_screenshot/006image.png)
![输入图片说明](Project_screenshot/007image.png)

##### 4.  登录用户2可进行竞价

![输入图片说明](Project_screenshot/008image.png)
![输入图片说明](Project_screenshot/009image.png)

##### 5.  用户2可以在我的-竞价中查看已竞价商品，用户1可以在发布-有人出价查看

![输入图片说明](Project_screenshot/010image.png)
![输入图片说明](Project_screenshot/011image.png)

##### 6.  用户1右划商品可以手动结束拍卖

![输入图片说明](Project_screenshot/012image.png)
![输入图片说明](Project_screenshot/013image.png)
![输入图片说明](Project_screenshot/014image.png)

##### 7.  查看交易完成订单用户1在我的-交易完成-我卖出的，用户2在我的-交易完成-我买到的

![输入图片说明](Project_screenshot/015image.png)
![输入图片说明](Project_screenshot/016image.png)

##### 8.  用户2可右划进行支付

![输入图片说明](Project_screenshot/017image.png)

##### 9.  跳转支付，支付宝沙箱支付页面

![输入图片说明](Project_screenshot/018image.png)
![输入图片说明](Project_screenshot/019image.png)

##### 10.  交易完成后标签变成绿色  

![输入图片说明](Project_screenshot/020image.png)

