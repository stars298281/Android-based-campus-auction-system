package com.example.auction.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.example.auction.R;
import com.example.auction.activity.AddItemActivity;
import com.example.auction.activity.UserUIBaseActivity;
import com.example.auction.adapter.MyFragmentTabAdapter;
import com.example.auction.adapter.PublishItemListAdapter;
import com.example.auction.bean.Item;
import com.example.auction.sqlite.Sqlite;
import com.example.auction.util.MyUtil;
import com.google.android.material.tabs.TabLayout;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PublishFragment extends Fragment {

    private ViewPager viewPager;
    //注意不要打成TableLayout
    private TabLayout tl_tab;
    //fragment数据源
    private List<Fragment> fragmentList;
    //标题数据源
    private List<String> titleList;
    //分类列表
    private String[] publishedList = new String[]{
            "我发布的", "有人出价"};

    //发布按钮
    private ImageButton btn_publish_item;

    private MyFragmentTabAdapter myTabAdapter;

    private Integer user_id;
    private Sqlite sqlite;
    private Cursor cursor;

    //是否第一次加载
    private boolean isFirstLoading = true;
    private String state;
    private Integer item_id;

    /**
     * 在fragment可见的时候，刷新数据
     */
    @Override
    public void onResume() {
        super.onResume();

        if (!isFirstLoading) {
            //如果不是第一次加载，刷新数据
            initData();
            inAdapter();
        }

        isFirstLoading = false;
    }

    public PublishFragment() {

    }

    //接收登录用户的id
    public PublishFragment(Integer userId) {
        this.user_id = userId;
    }

    public static PublishFragment newInstance(String param1, String param2) {
        PublishFragment fragment = new PublishFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_publish, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //fragment中的数据库对象要在onViewCreat中创建（fragment的生命周期）
        sqlite = new Sqlite((UserUIBaseActivity) getActivity());

        viewPager = view.findViewById(R.id.vp_tab_layout);
        tl_tab = view.findViewById(R.id.tl_tab);

        btn_publish_item = view.findViewById(R.id.btn_publish_item);

        //初始化数据
        initData();
        //加载适配器
        inAdapter();
    }

    //加载适配器
    private void inAdapter() {
        /*
          这是fragment中的子fragment，子fragment的FragmentManager是他的父fragment，
          所以要getChildFragmentManager，而不是getSupportFragmentManager，
          并且参数多了标题列表
        */
        myTabAdapter = new MyFragmentTabAdapter(getChildFragmentManager(),
                fragmentList,
                titleList);

        //装载适配器
        viewPager.setAdapter(myTabAdapter);

        //将顶部标题装载进viewPager
        tl_tab.setupWithViewPager(viewPager);

        //加号发布按钮
//        btn_publish_item.setOnClickListener(new jumpAddItem());

    }

    @SuppressLint("Range")
    //查询并插入已发布的拍卖品
    private void initData() {

        btn_publish_item.setOnClickListener(new jumpAddItem());

        fragmentList = new ArrayList<>();

        TabLayoutPublishedItemFragment fragment1 = new TabLayoutPublishedItemFragment(user_id, publishedList[0]);
        TabLayoutPublishedItemFragment fragment2 = new TabLayoutPublishedItemFragment(user_id, publishedList[1]);

        fragmentList.add(fragment1);
        fragmentList.add(fragment2);

        //装载标签
        titleList = new ArrayList<>();
        for (int i = 0; i < publishedList.length; i++) {
            titleList.add(publishedList[i]);
        }

    }


    //跳转到发布页
    private class jumpAddItem implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), AddItemActivity.class);
            intent.putExtra("user_id", user_id);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

}