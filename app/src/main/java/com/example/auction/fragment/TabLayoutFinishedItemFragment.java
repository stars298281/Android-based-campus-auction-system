package com.example.auction.fragment;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.auction.R;
import com.example.auction.activity.mineActivity.MineFinishedActivity;
import com.example.auction.adapter.FinishedItemListAdapter;
import com.example.auction.bean.Item;
import com.example.auction.sqlite.Sqlite;
import com.example.auction.util.MyUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class TabLayoutFinishedItemFragment extends Fragment {

    //分类商品展示
    private ListView lv_finished_item_list;
    private TextView tv_list_none;

    //自己重写图文列表适配器
    private MineFinishedActivity mineFinishedActivity;

    //数据源的列表的类型是Map类型（键值对），Map<String（键名），Object（值的类型）>
    private List<Item> finishedItemList;

    private Integer user_id;
    private Integer item_id;
    private Sqlite sqlite;
    private Cursor cursor;

    //类别名字
    private String finished;

    //是否第一次加载
    private boolean isFirstLoading = true;

    /**
     * 在fragment可见的时候，刷新数据
     */
    @Override
    public void onResume() {
        super.onResume();

        if (!isFirstLoading) {
            //如果不是第一次加载，刷新数据
            initData();
            inAdapter();
        }

        isFirstLoading = false;
    }

    public TabLayoutFinishedItemFragment() {

    }

    //接受数据的构造方法
    public TabLayoutFinishedItemFragment(Integer userId, String finished) {
        this.user_id = userId;
        this.finished = finished;
        if (userId != 0 && !finished.equals("")) {

        }
    }

    public static TabLayoutFinishedItemFragment newInstance(String param1, String param2) {
        TabLayoutFinishedItemFragment fragment = new TabLayoutFinishedItemFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_layout_finished_item, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        user_id = 2;
        //fragment中的数据库对象要在onViewCreat中创建（fragment的生命周期）
        sqlite = new Sqlite(getActivity());
        lv_finished_item_list = view.findViewById(R.id.lv_finished_item_list);
        tv_list_none = view.findViewById(R.id.tv_list_none);

        //初始化数据
        initData();
        inAdapter();
    }

    private void inAdapter() {
        //初始化适配器
        FinishedItemListAdapter finishedItemListAdapter = new FinishedItemListAdapter(
                getActivity(), finishedItemList, user_id, finished
        );

        //装载适配器
        lv_finished_item_list.setAdapter(finishedItemListAdapter);
    }

    @SuppressLint("Range")
    private void initData() {
        //已完成的拍卖品列表
        finishedItemList = new ArrayList<>();

        //分开我买到的和我卖出的物品查询
        if (finished.equals("我买到的")) {
            cursor = sqlite.selectFinishedItem(
                    "winner_id", user_id.toString(),
                    "winner_visible", 1,
                    "已结束");
//            cursor = sqlite.selectItem("item_id",Integer.valueOf(2).toString(),"item_id desc");
        } else if (finished.equals("我卖出的")) {
            cursor = sqlite.selectFinishedItem(
                    "owner_id", user_id.toString(),
                    "owner_visible", 1,
                    "已结束");
        }

        if (cursor.getCount() > 0) {
/*
            //[1]显示
            View对象.setVisibility(View.VISIBLE);
            //[2]隐藏 但是会占据到空间的大小
            View对象.setVisibility(View.INVISIBLE);
            //[3]隐藏 但是不会占据空间的大小
            View对象.setVisibility(View.GONE);
*/
            tv_list_none.setVisibility(View.GONE);

        } else {
            tv_list_none.setVisibility(View.VISIBLE);
        }
        while (cursor.moveToNext()) {

            Item item = new Item();

            //根据id获取前图片
            byte[] image = cursor.getBlob(cursor.getColumnIndex("item_image"));

            //物品名
            String itemName = cursor.getString(cursor.getColumnIndex("item_name"));
            //描述
            String itemDes = cursor.getString(cursor.getColumnIndex("item_desc"));
            //价格
            String maxPrice = cursor.getString(cursor.getColumnIndex("max_price"));
            //发布者名字
            Integer owner_id = 0;
            owner_id = cursor.getInt(cursor.getColumnIndex("owner_id"));

            Integer winner_id = cursor.getInt(cursor.getColumnIndex("winner_id"));

            String ownerName = MyUtil.getUserNameByID(getActivity(), owner_id);
            //截止时间
            String endTime = cursor.getString(cursor.getColumnIndex("endtime"));

            Integer owner_visible = cursor.getInt(cursor.getColumnIndex("owner_visible"));
            //状态
            String state = cursor.getString(cursor.getColumnIndex("state"));

            item_id = cursor.getInt(cursor.getColumnIndex("item_id"));

            //数据用javabean存储
            item.setItemId(item_id);


            item.setWinnerId(winner_id);

            item.setOwnerId(owner_id);

            item.setImage(image);
            item.setItemName(itemName);
            item.setItemDesc(itemDes);
            item.setEndtime(endTime);

            //发布时的最高价等于发布价
            item.setMaxPrice(maxPrice);
            item.setOwnerVisible(owner_visible);
            item.setState(state);

            //将子项加进列表
            finishedItemList.add(item);
        }
//        if (cursor != null && cursor.moveToFirst()){
//            tv_list_none.setVisibility(View.VISIBLE);
//        }else {
//            tv_list_none.setVisibility(View.GONE);
//        }

    }
}