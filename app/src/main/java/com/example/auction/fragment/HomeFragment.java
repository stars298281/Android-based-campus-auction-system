package com.example.auction.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.auction.R;
import com.example.auction.adapter.MyFragmentTabAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

//首页fragment，加载分类标签
public class HomeFragment extends Fragment {

    private ViewPager viewPager;
    //注意不要打成TableLayout
    private TabLayout tl_top_nav;
    //fragment数据源
    private List<Fragment> fragmentList;
    //标题数据源
    private List<String> titleList;
    //分类列表
    private String[] kindlist = new String[]{
            "全部", "数码", "书籍", "日用", "文具", "服饰", "箱包", "鞋子", "游戏"};

    //从activity传来的数据
    private Integer user_id = 0;

    private MyFragmentTabAdapter myTabAdapter;


    public HomeFragment() {

    }

    //接受数据的构造方法
    public HomeFragment(Integer userId) {
        this.user_id = userId;
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager = view.findViewById(R.id.vp_tab_layout);
        tl_top_nav = view.findViewById(R.id.tl_top_nav);

        //初始化fragment数据和title数据
        initData();
        initAdapter();
    }

    private void initAdapter() {
        /*
          这是fragment中的子fragment，子fragment的FragmentManager是他的父fragment，
          所以要getChildFragmentManager，而不是getSupportFragmentManager，
          并且参数多了标题列表
        */
        myTabAdapter = new MyFragmentTabAdapter(getChildFragmentManager(),
                fragmentList,
                titleList);

        //装载适配器
        viewPager.setAdapter(myTabAdapter);

        //将顶部标题装载进viewPager
        tl_top_nav.setupWithViewPager(viewPager);
    }

    private void initData() {
        fragmentList = new ArrayList<>();

        //加载标签具体内容fragment
        TabLayoutKindItemFragment fragment1 = new TabLayoutKindItemFragment(user_id, kindlist[0]);
        TabLayoutKindItemFragment fragment2 = new TabLayoutKindItemFragment(user_id, kindlist[1]);
        TabLayoutKindItemFragment fragment3 = new TabLayoutKindItemFragment(user_id, kindlist[2]);
        TabLayoutKindItemFragment fragment4 = new TabLayoutKindItemFragment(user_id, kindlist[3]);
        TabLayoutKindItemFragment fragment5 = new TabLayoutKindItemFragment(user_id, kindlist[4]);
        TabLayoutKindItemFragment fragment6 = new TabLayoutKindItemFragment(user_id, kindlist[5]);
        TabLayoutKindItemFragment fragment7 = new TabLayoutKindItemFragment(user_id, kindlist[6]);
        TabLayoutKindItemFragment fragment8 = new TabLayoutKindItemFragment(user_id, kindlist[7]);
        TabLayoutKindItemFragment fragment9 = new TabLayoutKindItemFragment(user_id, kindlist[8]);

        //添加标签
        fragmentList.add(fragment1);
        fragmentList.add(fragment2);
        fragmentList.add(fragment3);
        fragmentList.add(fragment4);
        fragmentList.add(fragment5);
        fragmentList.add(fragment6);
        fragmentList.add(fragment7);
        fragmentList.add(fragment8);
        fragmentList.add(fragment9);

        //装载标签
        titleList = new ArrayList<>();
        for (int i = 0; i < kindlist.length; i++) {
            titleList.add(kindlist[i]);
        }

    }
}