package com.example.auction.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager.widget.ViewPager;
import com.example.auction.R;
import com.example.auction.adapter.MyFragmentTabAdapter;
import com.google.android.material.tabs.TabLayout;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdminReviewFragment extends Fragment {

    private ViewPager viewPager;
    //注意不要打成TableLayout
    private TabLayout tl_top_nav;
    //fragment数据源
    private List<Fragment> fragmentList;
    //标题数据源
    private List<String> titleList;
    //分类列表
    private String[] statelist = new String[]{
            "审核中", "已通过", "不通过","已结束"};

    //从activity传来的数据
    private Integer admin_id = 1;

    private MyFragmentTabAdapter myTabAdapter;


    public AdminReviewFragment() {

    }

    //接受数据的构造方法
    public AdminReviewFragment(Integer userId) {
        this.admin_id = userId;
    }
    
    public static AdminReviewFragment newInstance(String param1, String param2) {
        AdminReviewFragment fragment = new AdminReviewFragment();
        Bundle args = new Bundle();
       
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        
        return inflater.inflate(R.layout.fragment_admin_review, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager = view.findViewById(R.id.vp_tab_layout);
        tl_top_nav = view.findViewById(R.id.tl_top_nav);

        //初始化fragment数据和title数据
        initData();
        /*
          这是fragment中的子fragment，子fragment的FragmentManager是他的父fragment，
          所以要getChildFragmentManager，而不是getSupportFragmentManager，
          并且参数多了标题列表
        */
        myTabAdapter = new MyFragmentTabAdapter(getChildFragmentManager(),
                fragmentList,
                titleList);

        //装载适配器
        viewPager.setAdapter(myTabAdapter);

        //将顶部标题装载进viewPager
        tl_top_nav.setupWithViewPager(viewPager);
    }

    private void initData() {
        fragmentList = new ArrayList<>();

        TabLayoutStateItemFragment fragment1 = new TabLayoutStateItemFragment(admin_id, statelist[0]);
        TabLayoutStateItemFragment fragment2 = new TabLayoutStateItemFragment(admin_id, statelist[1]);
        TabLayoutStateItemFragment fragment3 = new TabLayoutStateItemFragment(admin_id, statelist[2]);
        TabLayoutStateItemFragment fragment4 = new TabLayoutStateItemFragment(admin_id, statelist[3]);
        
        fragmentList.add(fragment1);
        fragmentList.add(fragment2);
        fragmentList.add(fragment3);
        fragmentList.add(fragment4);

        //装载标签
        titleList = new ArrayList<>();
        for (int i = 0; i < statelist.length; i++) {
            titleList.add(statelist[i]);
        }
    }
}