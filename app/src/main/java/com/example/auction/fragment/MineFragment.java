package com.example.auction.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.auction.R;
import com.example.auction.activity.LoginActivity;
import com.example.auction.activity.mineActivity.*;
import com.example.auction.sqlite.Sqlite;
import com.example.auction.util.MyUtil;
import org.jetbrains.annotations.NotNull;


public class MineFragment extends Fragment {

    private Integer user_id;

    private RelativeLayout rl_review, rl_published, rl_bidding, rl_closed, rl_finish;
    private Button btn_edit, btn_exit_login;
    private ImageView iv_user_avatar;

    private Sqlite sqlite;
    private Cursor cursor;
    private TextView tv_top_name;

    //是否第一次加载
    private boolean isFirstLoading = true;

    /**
     * 在fragment可见的时候，刷新数据
     */
    @Override
    public void onResume() {
        super.onResume();

        if (!isFirstLoading) {
            //如果不是第一次加载，刷新数据
            initData();
            initOnClick();
        }

        isFirstLoading = false;
    }


    public MineFragment() {

    }

    public MineFragment(Integer userId) {
        this.user_id = userId;
    }

    public static MineFragment newInstance(String param1, String param2) {
        MineFragment fragment = new MineFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_mine, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sqlite = new Sqlite(getActivity());

        findViewId(view);
        initData();
        initOnClick();


    }

    //所有点击44监听
    private void initOnClick() {
        //审核中
        rl_review.setOnClickListener(new rlOnClick());
        //已发布
//        rl_published.setOnClickListener(new rlOnClick());
        //竞价中
        rl_bidding.setOnClickListener(new rlOnClick());
        //已结束
        rl_closed.setOnClickListener(new rlOnClick());
        //交易完成
        rl_finish.setOnClickListener(new rlOnClick());

        //修改信息
        btn_edit.setOnClickListener(new rlOnClick());
        //退出登录
        btn_exit_login.setOnClickListener(new rlOnClick());
    }

    @SuppressLint("Range")
    private void initData() {
        //根据拍卖品名称查找item表
        cursor = sqlite.selectUser(user_id);
        //获取头像
        Bitmap imagebm = MyUtil.getImageBySQL(getActivity(), "user", user_id);
        iv_user_avatar.setImageBitmap(imagebm);
        if (cursor.moveToNext()){
            String user_name = cursor.getString(cursor.getColumnIndex("user_name"));
            tv_top_name.setText(user_name);
        }
    }

    private void findViewId(View view) {

        iv_user_avatar = view.findViewById(R.id.iv_user_avatar);
        tv_top_name = view.findViewById(R.id.tv_top_name);

        //审核
        rl_review = view.findViewById(R.id.rl_review);
        //已发布
//        rl_published = view.findViewById(R.id.rl_published);
        //竞价中
        rl_bidding = view.findViewById(R.id.rl_bidding);
        //已结束
        rl_closed = view.findViewById(R.id.rl_closed);
        //交易完成
        rl_finish = view.findViewById(R.id.rl_finish);

        btn_edit = view.findViewById(R.id.btn_edit);
        btn_exit_login = view.findViewById(R.id.btn_exit_login);
    }

    private class rlOnClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            switch (v.getId()) {
                //审核
                case R.id.rl_review:
                    intent.setClass(getActivity(), MineReviewActivity.class);
                    intent.putExtra("user_id",user_id);
                    startActivity(intent);
                    break;
                //竞价中
                case R.id.rl_bidding:
                    intent.setClass(getActivity(), MineBiddingActivity.class);
                    intent.putExtra("user_id",user_id);
                    startActivity(intent);
                    break;
                //已结束
                case R.id.rl_closed:
                    intent.setClass(getActivity(), MineClosedActivity.class);
                    intent.putExtra("user_id",user_id);
                    startActivity(intent);
                    break;

                //交易完成
                case R.id.rl_finish:
                    intent.setClass(getActivity(), MineFinishedActivity.class);
                    intent.putExtra("user_id",user_id);
                    startActivity(intent);
                    break;

                //修改信息
                case R.id.btn_edit:
                    intent.setClass(getActivity(), MineEditActivity.class);
                    intent.putExtra("user_id",user_id);
                    startActivity(intent);
                    break;

                //退出登录
                case R.id.btn_exit_login:
                    intent.setClass(getActivity(), LoginActivity.class);
                    SharedPreferences spf = getActivity().getSharedPreferences("loginData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = spf.edit();
                    //取消自动登录
                    edit.putBoolean("isAutoLogin", false);
                    edit.apply();
                    startActivity(intent);
                    getActivity().finish();
                    break;
            }

        }
    }
}