package com.example.auction.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.auction.R;
import com.example.auction.sqlite.Sqlite;
import com.example.auction.util.ItemTimeOut;
import com.example.auction.util.MyUtil;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

//开屏页
public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";
    private static final int sleepTime = 1000;

    private Sqlite sqlite = new Sqlite(this);
    private String[] kindlist = new String[]{
            "全部", "数码", "书籍", "日用", "文具", "服饰", "箱包", "鞋子", "游戏"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ItemTimeOut.checkItem(this);
        init();
    }

    @SuppressLint("ResourceType")
    public void init() {
        String adminName = "root";
        String adminPass = "root";
        Integer isAdmin = 1;

        Cursor cursor = sqlite.selectUser(adminName, isAdmin);
        int number = 0;
        number = cursor.getCount();

        //如果第一次进入app
        if (number == 0) {
            //此处为判断首次进入app（即首次安装app后），如果是第一次可以进行一些操作
            //插入类别到类别表
//            initKindList();

            //默认头像
            InputStream is = getResources().openRawResource(R.drawable.test_img3);
            Bitmap mBitmap = BitmapFactory.decodeStream(is);
            //转成byte类型存进数据库
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            //转换成byte才能存进数据库
            byte[] image = baos.toByteArray();

            //管理员不存在则注册
            long i = sqlite.insertUserAdmin(image,adminName, adminPass, isAdmin);

            //插入n个拍卖品数据
            MyUtil.insertItemData(SplashActivity.this,20);
            //插入1~3个用户
            MyUtil.insertUserData(SplashActivity.this,3);

            // i为插入了几行
            if (i >= 1) {
                Toast.makeText(getApplicationContext(), "初始化成功1", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "初始化失败", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "初始化成功2", Toast.LENGTH_SHORT).show();
        }
        initLoading();
    }

    //开屏页方法
    private void initLoading() {
        new Thread(new Runnable() {
            public void run() {
                long start = System.currentTimeMillis();
                long costTime = System.currentTimeMillis() - start;
                if (sleepTime > costTime ) {
                    try {
                        Thread.sleep(sleepTime - costTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //进入主页面
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }).start();
    }

//    private void initKindList() {
//        for (int i = 0; i < kindlist.length; i++) {
//            sqlite.insertKind(kindlist[i], null);
////            Toast.makeText(
////                    SplashActivity.this,
////                    kindlist[i] + "添加成功",
////                    Toast.LENGTH_SHORT).show();
//        }
//    }

}