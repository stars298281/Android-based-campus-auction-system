package com.example.auction.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.example.auction.R;
import com.example.auction.adapter.MyFragmentVPAdapter;
import com.example.auction.fragment.AdminReviewFragment;
import com.example.auction.util.ItemTimeOut;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class AdminUIBaseActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private BottomNavigationView bottomNav;
    private MyFragmentVPAdapter myStateVPAdapter;
    private List<Fragment> fragmentList;

    private final Integer ADMIN_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_ui_base);

        //刷新物品状态
        ItemTimeOut.checkItem(this);

        ImageView iv_exit_login = findViewById(R.id.iv_exit_login);
        viewPager = findViewById(R.id.vp_bottomnavigation);
        bottomNav = findViewById(R.id.bnav_menu);

        //初始化页面数据源
        initData();

        myStateVPAdapter = new MyFragmentVPAdapter(getSupportFragmentManager(), fragmentList);

        viewPager.setAdapter(myStateVPAdapter);
        iv_exit_login.setOnClickListener(new exitLogin());
    }

    //初始化fragment数据源
    private void initData() {
        fragmentList = new ArrayList<>();
        //获取admin_id
//        admin_id = getIntent().getExtras().getInt("user_id");

        //底部导航对应的fragment
        AdminReviewFragment fragmentHOME = new AdminReviewFragment(ADMIN_ID);
        fragmentList.add(fragmentHOME);
    }

    //退出登录
    private class exitLogin implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClass(AdminUIBaseActivity.this, LoginActivity.class);
            SharedPreferences spf = AdminUIBaseActivity.this.getSharedPreferences("loginData", Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = spf.edit();
            //取消自动登录
            edit.putBoolean("isAutoLogin", false);
//            edit.putInt("adminNum", 0);
            edit.apply();
            startActivity(intent);
            AdminUIBaseActivity.this.finish();
        }
    }
}