package com.example.auction.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.auction.R;
import com.example.auction.sqlite.Sqlite;
import com.example.auction.util.MyUtil;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class RegisterActivity extends Activity {
    private Button btn_reg = null, btn_back_login = null;
    private EditText username = null, userpass = null, userphone = null;
    private ImageView mClearUserNameView, mClearPasswordView, mClearPhoneView;
    private String regname = "", regpass = "", regphone = "";

    //admin为0是用户权限，为1是管理员权限
    private Integer admin = 0;

    Sqlite sqlite = new Sqlite(RegisterActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        findViewId();

        //清空输入按钮
        mClearUserNameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username.setText("");
            }
        });
        mClearPasswordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userpass.setText("");
            }
        });
        mClearPhoneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userphone.setText("");
            }
        });

        //返回登录按钮
        btn_back_login.setOnClickListener(new backLogin());

        //注册按钮
        btn_reg.setOnClickListener(new regClick());
    }

    private void findViewId() {
        btn_reg = (Button) findViewById(R.id.btn_reg);
        btn_back_login = (Button) findViewById(R.id.back_login);

        username = (EditText) findViewById(R.id.username_text1);
        userpass = (EditText) findViewById(R.id.userpass_text1);
        userphone = (EditText) findViewById(R.id.phone_text1);

        mClearUserNameView = findViewById(R.id.iv_clear_username);
        mClearPasswordView = findViewById(R.id.iv_clear_password);
        mClearPhoneView = findViewById(R.id.iv_clear_phone);
    }

    private class backLogin implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // 跳回登陆界面
            Intent intent = new Intent();
            intent.setClass(RegisterActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    // 查找用户名
    private String checkname(String regName) {
        String str = "";
        //在用户表中查找注册名字
        Cursor cursor = sqlite.selectUser(regName);
        if (cursor != null && cursor.moveToFirst()) {
//            //获取用户表列名username
//            str = cursor.getString(cursor.getColumnIndex("username"));
            str = "false";
        }
        return str;
    }

    @SuppressLint("ResourceType")
    private class regClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            regname = username.getText().toString().trim();
            regpass = userpass.getText().toString().trim();
            regphone = userphone.getText().toString().trim();
            if ((regname.equals("")) || (regpass.equals("")) || (regphone.equals(""))) {
                Toast.makeText(getApplicationContext(), "请将信息填写完整", Toast.LENGTH_SHORT).show();
            } else {
                // 确定是否存在该用户名
                //默认头像
                byte[] image = MyUtil.getDrawableImgToByte(RegisterActivity.this, R.drawable.test_img5);

                if (checkname(regname).equals("")) {
                    long i = sqlite.insertUser(image, regname, regpass, regphone);
                    // i为插入了几行
                    if (i >= 1) {
                        Toast.makeText(getApplicationContext(), "注册成功", Toast.LENGTH_SHORT).show();
                        // 跳回登陆界面
                        new backLogin();
                        // 让注册的activity生命期结束
                        RegisterActivity.this.finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "注册失败", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "用户名已存在", Toast.LENGTH_SHORT).show();
                    username.setText("");
                    userpass.setText("");
                    userphone.setText("");
                    username.setSelection(0);
                }
            }
        }
    }
}
