package com.example.auction.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.example.auction.R;
import com.example.auction.sqlite.Sqlite;
import com.example.auction.util.AdminDialog;
import com.example.auction.util.ItemTimeOut;
import com.example.auction.util.MyUtil;

public class ItemReviewViewActivity extends AppCompatActivity {

    private TextView tvBack, tvOwnerName, tvItemName,
            tvItemDes, tvMarkupPrice, tvOwnerPhone;
    private ImageView ivItemImg, ivOwnerImg, ivOwnerImg2;

    private TextView tv_owner_init_price;
    private TextView tvOwnerAddTime;
    private TextView tvOwnerEndTime;
    private TextView tv_state;
    private TextView tv_owner_id;
    private TextView tv_item_id;

    private ImageButton ibtn_no_pass;
    private ImageButton ibtn_pass;
    private LinearLayout ll_bottom_nav;

    private AdminDialog reviewDialog;
    private Integer user_id = 0;
    private Integer owner_id = 0;
    private Integer item_id = 0;
    private Integer markupPrice = 0;

    Sqlite sqlite = new Sqlite(ItemReviewViewActivity.this);
    private Cursor cursor;
    //加价信标，大于0时才可以进行减价
    private int bidding;
    private Cursor cursor1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_review_view);

        //刷新物品状态
        ItemTimeOut.checkItem(this);

        findViewBy();
        initData();
        showItem();
        reviewBtns();
    }

    //通过和不通过按钮
    private void reviewBtns() {
        //返回按钮
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemReviewViewActivity.this.finish();
            }
        });
        //通过按钮
        ibtn_pass.setOnClickListener(new reviewPass());
        //不通过按钮
        ibtn_no_pass.setOnClickListener(new reviewPass());
    }

    @SuppressLint("Range")
    private void showItem() {
        if (cursor != null && cursor.moveToFirst()) {
            //发布者id
            owner_id = cursor.getInt(cursor.getColumnIndex("owner_id"));
            tv_owner_id.setText(owner_id.toString());
            //拍卖品id
            tv_item_id.setText(item_id.toString());

            //获取发布者名字
            String owner_name = MyUtil.getUserNameByID(ItemReviewViewActivity.this, owner_id);
            tvOwnerName.setText(owner_name);

            //获取拍卖品名称
            String item_name = cursor.getString(cursor.getColumnIndex("item_name"));
            tvItemName.setText(item_name);
            //获取拍卖品描述
            String item_desc = cursor.getString(cursor.getColumnIndex("item_desc"));
            tvItemDes.setText(item_desc);

            //根据id获取图片
            //拍卖品图片
            Bitmap imgBm = MyUtil.getImageBySQL(ItemReviewViewActivity.this, "item", item_id);
            ivItemImg.setImageBitmap(imgBm);
            //发布者头像
            Bitmap imgBm1 = MyUtil.getImageBySQL(ItemReviewViewActivity.this, "user", owner_id);
            ivOwnerImg.setImageBitmap(imgBm1);
            ivOwnerImg2.setImageBitmap(imgBm1);

            //加价幅度
            String markup_price = cursor.getString(cursor.getColumnIndex("markup_price"));
            tvMarkupPrice.setText(markup_price);
            markupPrice = Integer.parseInt(markup_price);

            //发布者手机
            String owner_phone = MyUtil.getUserPhoneByID(ItemReviewViewActivity.this, owner_id);
            tvOwnerPhone.setText(owner_phone);

            //发布价
            String init_price = cursor.getString(cursor.getColumnIndex("init_price"));
            tv_owner_init_price.setText(init_price);

            //发布时间
            String add_time = cursor.getString(cursor.getColumnIndex("addtime"));
            tvOwnerAddTime.setText(add_time);
            //结束时间
            String end_time = cursor.getString(cursor.getColumnIndex("endtime"));
            tvOwnerEndTime.setText(end_time);

        }
    }

    @SuppressLint("Range")
    private void initData() {
        item_id = Integer.parseInt(getIntent().getExtras().getString("item_id"));
        user_id = getIntent().getExtras().getInt("user_id");

        //根据拍卖品id查找item表
        cursor = sqlite.selectItem("item_id", item_id.toString(), "item_id desc");

        //获取从哪个页面跳转过来的信标
        String state = getIntent().getExtras().getString("state");

        //从审核中跳转过来的用户
        if (state.equals("审核中")) {
            ibtn_no_pass.setVisibility(View.VISIBLE);
            ibtn_pass.setVisibility(View.VISIBLE);
            //gone是隐藏且不占位
            tv_state.setVisibility(View.GONE);
        }
        //从其他（已通过、不通过、已结束）跳转过来的用户
        else {
            //详情页不同状态显示不同内容
            defaultState(state);

            //gone是隐藏且不占位
            ibtn_no_pass.setVisibility(View.GONE);
            ibtn_pass.setVisibility(View.GONE);
        }
    }

    private void findViewBy() {
        tvBack = findViewById(R.id.tv_back);

        ivItemImg = findViewById(R.id.iv_item_img);
        ivOwnerImg = findViewById(R.id.iv_owner_img);
        ivOwnerImg2 = findViewById(R.id.iv_owner_img2);

        tvOwnerName = findViewById(R.id.tv_owner_name);
        tvItemName = findViewById(R.id.tv_item_name);

        tvMarkupPrice = findViewById(R.id.tv_markup_price);
        tvItemDes = findViewById(R.id.tv_item_des);

        tvOwnerPhone = findViewById(R.id.tv_owner_phone);
        tv_owner_init_price = findViewById(R.id.tv_owner_init_price);
        tv_item_id = findViewById(R.id.tv_item_id);
        tv_owner_id = findViewById(R.id.tv_owner_id);

        tvOwnerAddTime = findViewById(R.id.tv_owner_add_time);
        tvOwnerEndTime = findViewById(R.id.tv_owner_end_time);

        ll_bottom_nav = findViewById(R.id.ll_bottom_nav);
        ibtn_no_pass = findViewById(R.id.ibtn_no_pass);
        tv_state = findViewById(R.id.tv_state);
        ibtn_pass = findViewById(R.id.ibtn_pass);
    }

    //详情页不同状态显示不同内容
    private void defaultState(String state) {
        tv_state.setVisibility(View.VISIBLE);
        tv_state.setText(state);
        if (state.equals("已通过")) {
            tv_state.setBackgroundResource(R.color.state_green);
            ll_bottom_nav.setBackgroundResource(R.color.state_green);
        } else if (state.equals("不通过")) {
            tv_state.setBackgroundResource(R.color.state_red);
            ll_bottom_nav.setBackgroundResource(R.color.state_red);
        } else if (state.equals("已结束")) {
            tv_state.setBackgroundResource(R.color.state_gray);
            ll_bottom_nav.setBackgroundResource(R.color.state_gray);
        }
    }

    //通过和不通过按钮监听
    private class reviewPass implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ibtn_no_pass:
                    reviewDialog = new AdminDialog(ItemReviewViewActivity.this, new AdminDialog.OnCloseListener() {
                        @Override
                        public void onClick(Dialog dialog, boolean confirm) {
                            updateItemState("不通过");
                            dialog.dismiss();
                        }
                    });
                    reviewDialog.setTitle("请输入不通过原因").show();
                    break;
                case R.id.ibtn_pass:
                    reviewDialog = new AdminDialog(ItemReviewViewActivity.this, new AdminDialog.OnCloseListener() {
                        @Override
                        public void onClick(Dialog dialog, boolean confirm) {
                            updateItemState("已通过");
                            dialog.dismiss();
                        }
                    });
                    reviewDialog.setTitle("提示").show();
                    reviewDialog.setContent("确定通过审核吗？").show();
                    break;
                default:
                    break;
            }
        }
    }

    private void updateItemState(String state) {
        if (state.equals("不通过")) {
            Toast.makeText(this, "不通过", Toast.LENGTH_SHORT).show();
            String item_remark = reviewDialog.getEditText();
            if (!item_remark.equals("") && item_remark != null) {
                long i = sqlite.updateItem(item_id, "item_remark", item_remark);
                i += sqlite.updateItem(item_id, "state", state);
                if (i > 1) {
                    Toast.makeText(ItemReviewViewActivity.this,
                            "操作成功", Toast.LENGTH_SHORT).show();
                    jumpAdminHome();
                } else {
                    Toast.makeText(ItemReviewViewActivity.this,
                            "操作失败", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(ItemReviewViewActivity.this,
                        "请输入不通过原因！", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "" + state, Toast.LENGTH_SHORT).show();

            long i = sqlite.updateItem(item_id, "state", state);
            if (i > 0) {
                Toast.makeText(ItemReviewViewActivity.this,
                        "操作成功", Toast.LENGTH_SHORT).show();
                jumpAdminHome();
            } else {
                Toast.makeText(ItemReviewViewActivity.this,
                        "操作失败", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //回到管理首页
    private void jumpAdminHome() {
        Intent intent = new Intent(ItemReviewViewActivity.this, AdminUIBaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        ItemReviewViewActivity.this.finish();
    }

}
