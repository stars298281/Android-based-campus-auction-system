package com.example.auction.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.example.auction.R;
import com.example.auction.adapter.MyFragmentVPAdapter;
import com.example.auction.fragment.HomeFragment;
import com.example.auction.fragment.MineFragment;
import com.example.auction.fragment.PublishFragment;
import com.example.auction.util.ItemTimeOut;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class UserUIBaseActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private BottomNavigationView bottomNav;
    private MyFragmentVPAdapter myStateVPAdapter;
    private List<Fragment> fragmentList;

    private Integer user_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_ui_base);

        //刷新物品状态
        ItemTimeOut.checkItem(this);

        viewPager = findViewById(R.id.vp_bottomnavigation);
        bottomNav = findViewById(R.id.bnav_menu);

        //初始化页面数据源
        initData();

        initAdapter();


    }

    private void initAdapter() {
        myStateVPAdapter = new MyFragmentVPAdapter(getSupportFragmentManager(),fragmentList);

        viewPager.setAdapter(myStateVPAdapter);
        //fragment页面滑动监听
        viewPager.addOnPageChangeListener(new onPageChange());

        //底部导航按钮点击监听
        bottomNav.setOnNavigationItemSelectedListener(new onNavItemSelected());

//        //设置显示消息红点
//        BadgeDrawable badge_publish = bottomNav.getOrCreateBadge(R.id.bnav_publish);
//        //设置红点消息数，不设置默认为小红点无数字
//        badge_publish.setNumber(1000);
//        //设置红点数字最多显示多少个字符，不设置则默认为4，即999+
//        badge_publish.setMaxCharacterCount(3);
//
//        //只显示消息红点
//        BadgeDrawable badge_mine = bottomNav.getOrCreateBadge(R.id.bnav_mine);
    }

    //初始化fragment数据源
    private void initData() {
        fragmentList = new ArrayList<>();
        //获取user_id
        user_id = getIntent().getExtras().getInt("user_id");

        //底部导航对应的fragment
        HomeFragment fragmentHOME = new HomeFragment(user_id);
        PublishFragment fragmentPUBLISH = new PublishFragment(user_id);
        MineFragment fragmentMINE = new MineFragment(user_id);

        fragmentList.add(fragmentHOME);
        fragmentList.add(fragmentPUBLISH);
        fragmentList.add(fragmentMINE);
    }

    //argument方法传递数据给fragment，fragment在onCreat中接收数据
    public void passDataByArgument(View view) {
        /*
        String str = "这是通过Argument向fragment传递的数据";

        //new一个需要接收数据的fragment
        HomeTabLayoutChildFragment dataToFragment = new HomeTabLayoutChildFragment();
        //打包数据
        Bundle bundle = new Bundle();
        bundle.putString("data", str);
        bundle.putInt("int_data", 2982);
        //通过.setArgument()传递数据
        dataToFragment.setArguments(bundle);
        */
    }

    //页面滑动监听方法
    private class onPageChange implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            Toast.makeText(UserUIBaseActivity.this,
//                    "这是页面" + (position + 1),
//                    Toast.LENGTH_SHORT).show();
            //底部栏跟随选中页面被选中
            onViewPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    //页面被选中监听，页面被选中底部按钮会改变状态
    private void onViewPageSelected(int position) {
        switch (position) {
            case 0:
                bottomNav.setSelectedItemId(R.id.bnav_home);
                break;
            case 1:
                bottomNav.setSelectedItemId(R.id.bnav_publish);
                //当页面被选中时，清除红点消息提醒
                bottomNav.removeBadge(R.id.bnav_publish);
                break;
            case 2:
                bottomNav.setSelectedItemId(R.id.bnav_mine);
                //当页面被选中时，清除红点消息提醒
                bottomNav.removeBadge(R.id.bnav_mine);
                break;
            default:
                break;
        }
    }

    //底部导航按钮点击监听，点击底部按钮会转到对应fragment
    private class onNavItemSelected implements BottomNavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()){
                case R.id.bnav_home:
                    //选中第几页fragment
                    viewPager.setCurrentItem(0);
                    break;
                case R.id.bnav_publish:
                    //选中第几页fragment
                    viewPager.setCurrentItem(1);
                    break;
                case R.id.bnav_mine:
                    //选中第几页fragment
                    viewPager.setCurrentItem(2);
                    break;
            }
            //返回true，底部按钮才会显示被选中的效果
            return true;
        }
    }


}