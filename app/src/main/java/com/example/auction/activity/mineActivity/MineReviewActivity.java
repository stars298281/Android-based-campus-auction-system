package com.example.auction.activity.mineActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import com.example.auction.R;
import com.example.auction.adapter.MyFragmentTabAdapter;
import com.example.auction.fragment.TabLayoutStateItemFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MineReviewActivity extends AppCompatActivity {

    private ViewPager viewPager;
    //注意不要打成TableLayout
    private TabLayout tl_top_nav;
    //fragment数据源
    private List<Fragment> fragmentList;
    //标题数据源
    private List<String> titleList;
    //分类列表
    private String[] statelist = new String[]{
            "审核中", "已通过", "不通过","已结束"};

    //从activity传来的数据
    private Integer user_id;

    private MyFragmentTabAdapter myTabAdapter;

    private FragmentManager fragmentManager = getSupportFragmentManager();
    private TextView tv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_review);

        viewPager = findViewById(R.id.vp_tab_layout);
        tl_top_nav = findViewById(R.id.tl_top_nav);
        tv_back = findViewById(R.id.tv_back);

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MineReviewActivity.this.finish();
            }
        });

        //初始化fragment数据和title数据
        initData();

        /*
          这是fragment中的子fragment，子fragment的FragmentManager是他的父fragment，
          所以要getChildFragmentManager，而不是getSupportFragmentManager，
          并且参数多了标题列表
        */
        myTabAdapter = new MyFragmentTabAdapter(fragmentManager,
                fragmentList,
                titleList);

        //装载适配器
        viewPager.setAdapter(myTabAdapter);

        //将顶部标题装载进viewPager
        tl_top_nav.setupWithViewPager(viewPager);
    }

    private void initData() {
        user_id = getIntent().getExtras().getInt("user_id");
//        user_id = 2;

        fragmentList = new ArrayList<>();

        TabLayoutStateItemFragment fragment1 = new TabLayoutStateItemFragment(user_id, statelist[0]);
        TabLayoutStateItemFragment fragment2 = new TabLayoutStateItemFragment(user_id, statelist[1]);
        TabLayoutStateItemFragment fragment3 = new TabLayoutStateItemFragment(user_id, statelist[2]);
        TabLayoutStateItemFragment fragment4 = new TabLayoutStateItemFragment(user_id, statelist[3]);

        fragmentList.add(fragment1);
        fragmentList.add(fragment2);
        fragmentList.add(fragment3);
        fragmentList.add(fragment4);

        //装载标签
        titleList = new ArrayList<>();
        for (int i = 0; i < statelist.length; i++) {
            titleList.add(statelist[i]);
        }
    }
}