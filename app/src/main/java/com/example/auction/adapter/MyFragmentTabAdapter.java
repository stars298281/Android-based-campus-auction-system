package com.example.auction.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/*
   FragmentStatePagerAdapter比FragmentPagerAdapter多了内存回收机制，
   当fragment划过之后就会回收内存，把更多内存留给当前fragment
 */
//回收内存机制有可能将传给fragment的数据清除掉，需要重新加载
public class MyFragmentTabAdapter extends FragmentPagerAdapter {
    //数据源列表
    private List<Fragment> mFragmentList;
    //子fragment标签
    private List<String> mTitleList;

    public MyFragmentTabAdapter(@NonNull FragmentManager fm,
                                List<Fragment> fragmentList,
                                List<String> titleList) {
        super(fm);
        this.mFragmentList = fragmentList;
        this.mTitleList = titleList;
    }

    @NonNull
    @Override
    //获取当前位置（当前处于哪个fragment）
    public Fragment getItem(int position) {
        return mFragmentList == null ? null : mFragmentList.get(position);
    }

    @Override
    //获取页数
    public int getCount() {
        return mFragmentList == null ? 0 : mFragmentList.size();
    }

    @Nullable
    @Override
    //获取标题
    public CharSequence getPageTitle(int position) {
        return mTitleList == null ? "" : mTitleList.get(position);
    }
}
