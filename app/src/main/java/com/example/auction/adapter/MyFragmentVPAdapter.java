package com.example.auction.adapter;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/*
   FragmentStatePagerAdapter比FragmentPagerAdapter多了内存回收机制，
   当fragment划过之后就会回收内存，把更多内存留给当前fragment
 */
//回收内存机制有可能将传给fragment的数据清除掉，需要重新加载
public class MyFragmentVPAdapter extends FragmentPagerAdapter {

    //数据源
    private List<Fragment> mFragmentList;

    public MyFragmentVPAdapter(@NonNull FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.mFragmentList = fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position) == null ? null : mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList == null ? 0 : mFragmentList.size();
    }
}
