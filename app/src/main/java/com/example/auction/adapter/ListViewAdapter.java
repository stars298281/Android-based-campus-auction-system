package com.example.auction.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;

import java.util.List;
import java.util.Map;

public class ListViewAdapter extends SimpleAdapter {

    //创建一个接口对象，用于类外设置和内部调用；
    private onClickListener mOnClickListeber = null;

    //创建方法不变
    public ListViewAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
    }

    //创建一个方法来在类外设置接口对象
    public void setOnClickListeber(onClickListener mOnClickListeber) {
        this.mOnClickListeber = mOnClickListeber;
    }

    //对getView方法重写，getView在ListView绘制时会调用
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //父类方法返回的View相当于单个item控件
        View item = super.getView(position, convertView, parent);

        //检查自己创建的接口是否为空，不为空就调用接口方法，这个接口在后面设计
        if (mOnClickListeber != null) {

            //相当于将ListView的item控件和该item的位置（第几个item）传入函数，
            //就可以通过该item和item的位置操作item和item中的子控件了
            mOnClickListeber.onClick(item, position);
        }

        // 一定要返回这个item,否则无法显示ListView
        return item;
    }

    //添加一个接口
    public interface onClickListener {
        //添加一个接口方法，处理item，及其子控件
        void onClick(View view, int position);
    }

}
