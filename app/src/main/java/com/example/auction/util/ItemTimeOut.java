package com.example.auction.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import com.example.auction.sqlite.Sqlite;

//判断所有拍卖品是否到期，到期则赋予"已结束"状态
public class ItemTimeOut {

    @SuppressLint("Range")
    public static void checkItem(Context context) {
        Sqlite sqlite = new Sqlite(context);
        Cursor cursor = sqlite.selectItem("state", "已通过", "item_id asc");

        while (cursor.moveToNext()) {
            Integer item_id = cursor.getInt(cursor.getColumnIndex("item_id"));
            String nowTime = MyUtil.getNowDateString();
            String endTime = cursor.getString(cursor.getColumnIndex("endtime"));

            if (nowTime.compareTo(endTime) > 0){
                long i = sqlite.updateItem(item_id,"state","已结束");
            }

        }

    }
}
