package com.example.auction.util;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.example.auction.R;

//管理员端自定义弹框
public class AdminDialog extends Dialog implements View.OnClickListener{

    private TextView tvTitle;
    private TextView tvContent;
    private EditText etInitContent;
    private TextView tvBottomLeft;
    private TextView tvBottomRight;

    private Context mContext;
    private String content;
    private OnCloseListener listener;
    private String positiveName;
    private String negativeName;
    private String title;

    public AdminDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    public AdminDialog(Context context, int themeResId, String content, OnCloseListener listener) {
        super(context, themeResId);
        this.mContext = context;
        this.content = content;
        this.listener = listener;
    }

    public AdminDialog(Context context, OnCloseListener listener) {
        super(context, R.style.dialog);
        this.mContext = context;
        this.listener = listener;
    }

    //更改标题
    public AdminDialog setTitle(String title){
        this.title = title;
        return this;
    }

    public AdminDialog setContent(String content){
        this.content = content;
        //弹框内容
        if(!TextUtils.isEmpty(content)){
            tvContent.setText(content);
            tvContent.setVisibility(View.VISIBLE);
            etInitContent.setVisibility(View.GONE);
        }
        return this;
    }

    public AdminDialog setLeftBotton(String name){
        this.positiveName = name;
        return this;
    }

    public AdminDialog setRightBotton(String name){
        this.negativeName = name;
        return this;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_common_layout);
        setCanceledOnTouchOutside(false);
        initView();
    }


    private void initView(){
        tvTitle = findViewById(R.id.tv_title);
        tvContent = findViewById(R.id.tv_content);
        etInitContent = findViewById(R.id.et_init_content);
        
        tvBottomLeft = findViewById(R.id.tv_bottom_left);
        tvBottomRight = findViewById(R.id.tv_bottom_right);

        tvContent.setText(content);
        tvBottomLeft.setOnClickListener(this);
        tvBottomRight.setOnClickListener(this);

        //弹框标题
        if(!TextUtils.isEmpty(title)){
            tvTitle.setText(title);
        }
        //左边按钮文本
        if(!TextUtils.isEmpty(negativeName)){
            tvBottomLeft.setText(negativeName);
        }
        //右边按钮文本
        if(!TextUtils.isEmpty(positiveName)){
            tvBottomRight.setText(positiveName);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //取消按钮
            case R.id.tv_bottom_left:
//                if(listener != null){
//                    listener.onClick(this, false);
//                }
                this.dismiss();
                break;

            //功能按钮
            case R.id.tv_bottom_right:
                if(listener != null){
                    listener.onClick(this, true);
                }
                break;
        }
    }

    public String getEditText() {
        return etInitContent.getText().toString().trim();
    }


    public interface OnCloseListener{
        void onClick(Dialog dialog, boolean confirm);
    }

}
