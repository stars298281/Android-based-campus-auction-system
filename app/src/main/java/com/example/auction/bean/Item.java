package com.example.auction.bean;

import java.util.Arrays;

public class Item {

    //拍卖品id
    private Integer itemId;
    //拍卖品图片
    private byte[] image;
    //拍卖品名称
    private String itemName;
    //拍卖品描述
    private String itemDesc;
    private String addtime;
    private String endtime;
    //起拍价
    private String initPrice;
    //加价幅度
    private String markupPrice;
    //最高价
    private String maxPrice;
    //发布者id
    private Integer ownerId;
    //类别
    private String kind;
    //最终得主id
    private Integer winnerId;
    //备注（不通过原因等）
    private String itemRemark;
    //发布者可见状态，1为可见，0为不可见
    private Integer ownerVisible;
    //发布者可见状态，1为可见，0为不可见
    private Integer winnerVisible;
    //审核状态：审核中（默认），已通过，不通过，已结束
    private String state;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getInitPrice() {
        return initPrice;
    }

    public void setInitPrice(String initPrice) {
        this.initPrice = initPrice;
    }

    public String getMarkupPrice() {
        return markupPrice;
    }

    public void setMarkupPrice(String markupPrice) {
        this.markupPrice = markupPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Integer getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(Integer winnerId) {
        this.winnerId = winnerId;
    }

    public String getItemRemark() {
        return itemRemark;
    }

    public void setItemRemark(String itemRemark) {
        this.itemRemark = itemRemark;
    }

    public Integer getOwnerVisible() {
        return ownerVisible;
    }

    public void setOwnerVisible(Integer ownerVisible) {
        this.ownerVisible = ownerVisible;
    }

    public Integer getWinnerVisible() {
        return winnerVisible;
    }

    public void setWinnerVisible(Integer winnerVisible) {
        this.winnerVisible = winnerVisible;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
