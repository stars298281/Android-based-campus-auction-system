package com.example.auction.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import com.example.auction.R;
import com.example.auction.bean.Item;

import java.io.ByteArrayOutputStream;

public class Sqlite extends SQLiteOpenHelper {

    public static final String DB_NAME = "auction.db";
    public static final int VERSION = 1;

    //表名常量
    private static final String USER_TABLE = "Auction_user";
//    private static final String KIND_TABLE = "Auction_kind";
    private static final String BID_TABLE = "Auction_bid";
    private static final String ITEM_TABLE = "Auction_item";

    public Sqlite(@Nullable Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public Sqlite(Context context, String name, CursorFactory cursorFactory, int version) {
        super(context, name, cursorFactory, version);
    }

    public Sqlite(Context context, String name, int version) {
        this(context, name, null, version);
    }

    public Sqlite(Context context, String name) {
        this(context, name, VERSION);
    }

    @Override
    // 建表
    public void onCreate(SQLiteDatabase db) {
        //用户表
        db.execSQL(
                "create table if not exists " + USER_TABLE +
                        "(user_id integer primary key autoincrement," +
                        "user_image blob," +
                        "user_name varchar(20) not null," +
                        "user_pass varchar(20) not null," +
                        "user_phone varchar(50)," +
                        "admin integer)");

        //物品类别表
//        db.execSQL(
//                "create table if not exists " + KIND_TABLE +
//                        "(kind_id integer primary key autoincrement," +
//                        "kind_name varchar(20) not null," +
//                        "kind_desc varchar(50))");
        //竞价表
        db.execSQL(
                "create table if not exists " + BID_TABLE +
                        "(bid_id integer primary key autoincrement," +
                        "item_id integer not null," +
                        "user_id integer not null," +
                        "bid_price varchar(20) not null," +
                        //交易是否完成，1为是，默认为0，需要买家点击确认收货才可以改为1
                        "bid_finish integer not null," +
                        "bid_date varchar(20))");
        //物品表
        db.execSQL(
                "create table if not exists " + ITEM_TABLE +
                        "(item_id integer primary key autoincrement," +
                        //物品图片
                        "item_image blob," +
                        //拍卖品名称
                        "item_name varchar(20) not null," +
                        //描述
                        "item_desc varchar(255)," +
                        "addtime varchar(100)," +
                        "endtime varchar(100)," +
                        //起拍价
                        "init_price varchar(20) not null," +
                        //加价幅度
                        "markup_price varchar(20)," +
                        //最高价
                        "max_price varchar(20)," +
                        //发布者id
                        "owner_id integer not null," +
                        //当前最高价者id
                        "winner_id integer," +
                        //类别
                        "kind varchar(50)," +
                        //备注（不通过原因等）
                        "item_remark varchar(50)," +
                        //发布者可见状态，1为可见，0为不可见
                        "owner_visible integer not null," +
                        //买家可见状态，1为可见，0为不可见
                        "winner_visible integer not null," +
                        //审核状态：审核中（默认），已通过，不通过，已结束
                        "state varchar(20) not null)");

    }

    //删除物品
    public long deleteItem(String columnName, String deleteData) {
        SQLiteDatabase database = getWritableDatabase();

        return database.delete(ITEM_TABLE,
                columnName + "=?",
                new String[]{deleteData.trim()});

    }

    //更新用户信息
    public long updateUser(Integer user_id, String columnName, String newData) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(columnName, newData);

        return database.update(
                USER_TABLE,
                values,
                "user_id=?",
                new String[]{user_id.toString()});
    }

    //更新用户头像
    public long updateUserImage(Integer user_id, byte[] user_image) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("user_image", user_image);

        return database.update(
                USER_TABLE,
                values,
                "user_id=?",
                new String[]{user_id.toString()});
    }

    //更新物品部分信息（最高价或审核状态等）
    //返回此次更新影响的数据条数
    public long updateItem(Integer item_id, String columnName, String newData) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(columnName, newData);

        return database.update(
                ITEM_TABLE,
                values,
                "item_id=?",
                new String[]{item_id.toString()});
    }

    //更新审核表的物品的审核状态
    public long updateReview(Integer item_id, String columnName, String state) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("state", state);

        return database.update(
                ITEM_TABLE,
                values,
                "item_id=?",
                new String[]{item_id.toString()});

    }

    //更新竞价表
    //返回此次更新影响的数据条数
    public long updateBid(Integer item_id, Integer user_id, String data_name, String data_value) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();
        //更新出价和时间
        values.put(data_name, data_value);

        return database.update(
                BID_TABLE,
                values,
                "item_id=? and user_id=?",
                new String[]{item_id.toString(), user_id.toString()});

    }

    // 物品表信息插入
    public long insertItem(Item item) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("item_image", item.getImage());
        values.put("item_name", item.getItemName());
        values.put("item_desc", item.getItemDesc());
        values.put("addtime", item.getAddtime());
        values.put("endtime", item.getEndtime());
        values.put("init_price", item.getInitPrice());
        values.put("markup_price", item.getMarkupPrice());
        values.put("max_price", item.getMaxPrice());
        values.put("owner_id", item.getOwnerId());
        values.put("kind", item.getKind());
        values.put("winner_id", item.getWinnerId());
        values.put("item_remark", item.getItemRemark());
        values.put("owner_visible", item.getOwnerVisible());
        values.put("winner_visible", item.getWinnerVisible());
        values.put("state", item.getState());

        //返回插入成功的行数，为-1则不成功
        long row = database.insert(ITEM_TABLE, null, values);
        return row;

    }

    // 用户表信息插入
    public long insertUser(byte[] user_image, String user_name, String user_pass, String phone) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("user_image", user_image);
        values.put("user_name", user_name);
        values.put("user_pass", user_pass);
        values.put("user_phone", phone);
        //0为普通用户，1为管理员
        values.put("admin", 0);
        long row = database.insert(USER_TABLE, null, values);
        return row;
    }

    //管理员信息插入
    public long insertUserAdmin(byte[] user_image, String admin_name, String admin_pass, Integer isAdmin) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("user_image", user_image);
        values.put("user_name", admin_name);
        values.put("user_pass", admin_pass);
        //0为普通用户，1为管理员
        values.put("admin", isAdmin);
        long row = database.insert(USER_TABLE, null, values);
        return row;
    }

    // 竞价表信息插入
    public long insertBid(Integer item_id, Integer user_id, String bid_price, Integer bid_finish, String bid_date) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("item_id", item_id);
        values.put("user_id", user_id);
        values.put("bid_price", bid_price);
        values.put("bid_finish", bid_finish);
        values.put("bid_date", bid_date);

        long row = database.insert(BID_TABLE, null, values);
        return row;

    }

    // 物品种类表信息插入
//    public long insertKind(String kind_name, String kind_desc) {
//
//        SQLiteDatabase database = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("kind_name", kind_name);
//        values.put("kind_desc", kind_desc);
//
//        long row = database.insert(KIND_TABLE, null, values);
//        return row;
//
//    }

    //获取用户头像或拍卖品展示图
    public Cursor selectByID(String table_name, String selection, Integer user_or_item_id) {
        //查询所有用户和管理员
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                table_name, null,
                selection + "=?",
                new String[]{Integer.toString(user_or_item_id)},
                null, null, null);
        return cursor;
    }

    // 用户表信息查询
    //根据名字查询用户
    public Cursor selectUser(String username) {
        //查询所有用户和管理员
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                USER_TABLE, null,
                "user_name=?",
                new String[]{username.trim()},
                null, null,
                null);
        return cursor;
    }

    //根据名字和是否管理员查询用户
    public Cursor selectUser(String username, Integer admin) {
        //只查询用户或管理员
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                USER_TABLE, null,
                "user_name=? and admin=?",
                new String[]{username.trim(), String.valueOf(admin).trim()},
                null, null,
                null);
        return cursor;
    }

    //根据id查询用户
    public Cursor selectUser(Integer user_id) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                USER_TABLE, null,
                "user_id=?",
                new String[]{Integer.toString(user_id)},
                null, null, null);
        return cursor;
    }

    //根据传入的列名和值返回物品
    public Cursor selectItem(String selectColumn, String selectValue, String orderBy) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                ITEM_TABLE,
                //需要返回数据的列名，String[]{列名1，列名2}，为null则是全部列
                null,
                //查询条件，多条件可以用or或者and,为null则是全部列
                selectColumn + "=?",
                //String数组中的值与上面的问号一一对应，有多个问号则按顺序一一对应
                new String[]{selectValue.trim()},
                //返回数据的分组排序方式，为null就是默认
                null, null,
                //升序或者降序排序，值为"列名 ASC"（升序是asc，降序是desc），可为null
                orderBy);
        return cursor;
    }

    //根据传入的列名、值、可见状态、值返回物品
    public Cursor selectItem(String selectColumn, String selectValue, String visible, Integer visibleValue, String orderBy) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                ITEM_TABLE,
                //需要返回数据的列名，String[]{列名1，列名2}，为null则是全部列
                null,
                //查询条件，多条件可以用or或者and,为null则是全部列
                selectColumn + "=? and " + visible + "=?",
                //String数组中的值与上面的问号一一对应，有多个问号则按顺序一一对应
                new String[]{selectValue.trim(), visibleValue.toString().trim()},
                //返回数据的分组排序方式，为null就是默认
                null, null,
                //升序或者降序排序，值为"列名 ASC"（升序是asc，降序是desc），可为null
                orderBy);
        return cursor;
    }

    //查询有人出价的商品
    public Cursor selectBidItem(String owner_id,String winner_id, String orderBy) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                ITEM_TABLE,
                //需要返回数据的列名，String[]{列名1，列名2}，为null则是全部列
                null,
                //查询条件，多条件可以用or或者and,为null则是全部列
                "owner_id=? and owner_visible=1 and winner_id!=?",
                //String数组中的值与上面的问号一一对应，有多个问号则按顺序一一对应
                new String[]{owner_id.trim(),winner_id.trim()},
                //返回数据的分组排序方式，为null就是默认
                null, null,
                //升序或者降序排序，值为"列名 ASC"（升序是asc，降序是desc），可为null
                orderBy);
        return cursor;
    }


    //交易完成表专用
    public Cursor selectFinishedItem(String selectColumn, String selectValue, String visible, Integer visibleValue, String state) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                ITEM_TABLE,
                //需要返回数据的列名，String[]{列名1，列名2}，为null则是全部列
                null,
                //查询条件，多条件可以用or或者and,为null则是全部列
                selectColumn + "=? and " + visible + "=? and state=?",
                //String数组中的值与上面的问号一一对应，有多个问号则按顺序一一对应
                new String[]{selectValue.trim(), visibleValue.toString().trim(), state},
                //返回数据的分组排序方式，为null就是默认
                null, null,
                //升序或者降序排序，值为"列名 ASC"（升序是asc，降序是desc），可为null
                "endtime desc");
        return cursor;
    }

    //根据状态查询物品表全部数据，并且排序
    public Cursor selectItemByState(String state, String orderBy) {
        SQLiteDatabase database = getReadableDatabase();

        //查询全部已通过审核的物品
        Cursor cursor = database.query(
                ITEM_TABLE,
                //需要返回数据的列名，String[]{列名1，列名2}，为null则是全部列
                null,
                //查询条件，多条件可以用or或者and,为null则是全部列
                "state=?",
                //String数组中的值与上面的问号一一对应，有多个问号则按顺序一一对应
                new String[]{state},
                //返回数据的分组排序方式，为null就是默认
                null, null,
                //升序或者降序排序，值为"列名 ASC"（升序是asc，降序是desc），可为null
                orderBy);
        return cursor;
    }

    //根据传入的列名和状态返回已通过的物品
    public Cursor selectItemByState(String selectColumn, String selectValue, String state, String orderBy) {
        SQLiteDatabase database = this.getReadableDatabase();

        Cursor cursor = database.query(
                ITEM_TABLE,
                //需要返回数据的列名，String[]{列名1，列名2}，为null则是全部列
                null,
                //查询条件，多条件可以用or或者and,为null则是全部列
                selectColumn + "=? and state=?",
                //String数组中的值与上面的问号一一对应，有多个问号则按顺序一一对应
                new String[]{selectValue.trim(), state},
                //返回数据的分组排序方式，为null就是默认
                null, null,
                //升序或者降序排序，值为"列名 ASC"（升序是asc，降序是desc），可为null
                orderBy);
        return cursor;
    }

    //查询物品表全部数据，并且排序
    public Cursor selectItem(String orderBy) {
        SQLiteDatabase database = getReadableDatabase();
        //查询全部
        Cursor cursor = database.query(
                ITEM_TABLE,
                //需要返回数据的列名，String[]{列名1，列名2}，为null则是全部列
                null,
                //查询条件，多条件可以用or或者and,为null则是全部列
                null,
                //String数组中的值与上面的问号一一对应，有多个问号则按顺序一一对应
                null,
                //返回数据的分组排序方式，为null就是默认
                null, null,
                //升序或者降序排序，值为"列名 ASC"（升序是asc，降序是desc），可为null
                orderBy);
        return cursor;
    }

    //条件查询竞价表
    public Cursor selectBid(String columnName, String value) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                BID_TABLE, null,
                columnName + "=?",
                new String[]{value},
                null, null, "bid_id desc");
        return cursor;
    }

    //根据用户id和物品id查询是否竞价过该物品
    public Cursor selectBid(Integer item_id, Integer user_id) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                BID_TABLE, null,
                "item_id=? and user_id=?",
                new String[]{Integer.toString(item_id), Integer.toString(user_id)},
                null, null, "bid_id desc");
        return cursor;
    }

    //根据用户id和物品id查询是否竞价过该物品
    public Cursor selectBid(Integer item_id, Integer user_id, Integer bid_finish) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(
                BID_TABLE, null,
                "item_id=? and user_id=? and bid_finish=?",
                new String[]{item_id.toString(), user_id.toString(), bid_finish.toString()},
                null, null, "bid_id desc");
        return cursor;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
